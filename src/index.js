import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/font.css'
import './styles/common.css'

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

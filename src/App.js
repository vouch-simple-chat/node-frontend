import React, { Component } from 'react'
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import joinRoom from './pages/joinRoom'
import roomChat from './pages/roomChat'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={joinRoom} exact />
            <Route path="/join-room" component={joinRoom} exact />
            <Route path="/room-chat/" component={roomChat} exact />
          </Switch>
        </main>
      </BrowserRouter>

    )
  }
}

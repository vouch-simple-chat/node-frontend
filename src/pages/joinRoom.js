
import React, { Component } from 'react'
import axios from 'axios'
import utils from './utils'

export default class joinRoom extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            roomId: '',
            alertUsername: '',
            alertRoomId: '',
            errorAlert: ''
        }
    }

    onChangeUsername = (e) => {
        e.preventDefault();
        this.setState({ username: e.target.value })
    }

    onChangeRoomId = (e) => {
        e.preventDefault();
        this.setState({ roomId: e.target.value })
    }

    onJoin = (e) => {
        e.preventDefault();
        if (this.state.username === '') {
            this.setState({ alertUsername: '*) This Username field cannot be empty' })
        }
        if (this.state.roomId === '') {
            this.setState({ alertRoomId: '*) This RoomId field cannot be empty' })
        }
        if (this.state.username === '' && this.state.roomId === '') {
            return
        }
        if (this.state.username !== '') {
            this.setState({ alertUsername: '' })
        }
        if (this.state.roomId !== '') {
            if (this.state.roomId.length !== 5) {
                return this.setState({ alertRoomId: '*) This RoomId field must be 5 digit number' })
            }
            this.setState({ alertRoomId: '' })
        }
        if (this.state.username === '' || this.state.roomId === '') {
            return
        }
        const { username, roomId } = this.state
        const url = utils.BACKEND_URL + '/join-room'

        axios({ url, method: 'post', data: { username, roomId } })
            .then(() => {
                localStorage.setItem("username", username)
                localStorage.setItem("roomId", roomId)
                this.props.history.push("/room-chat");
                window.location.reload();
            })
            .catch(err => { this.setState({ errorAlert: err.response.data.message }) })
    }

    render() {
        return (
            <div className="centerGrid">
                <form className="form_login">
                    <p className="title">Join Chatroom</p>
                    <input className="formBox" placeholder="Username" onChange={this.onChangeUsername} onSubmit={event => event.preventDefault()}></input>
                    {
                        this.state.alertUsername !== '' ? <p className="alert">{this.state.alertUsername}</p> : null
                    }
                    <input className="formBox" placeholder="RoomId (to test use: 08527)" onChange={this.onChangeRoomId} onSubmit={event => event.preventDefault()}></input>
                    {
                        this.state.alertRoomId !== '' ? <p className="alert">{this.state.alertRoomId}</p> : null
                    }
                    {
                        this.state.errorAlert === '' &&
                        <button className="joinBox" onClick={this.onJoin}>JOIN</button>
                    }
                    {
                        this.state.errorAlert === 'Room Chat Not Found' &&
                        <div>
                            <p className="errorAlert">This Room {this.state.roomId} is never created</p>
                            <button className="cancelButton" onClick={e => {
                                e.preventDefault()
                                this.props.history.push("/");
                                window.location.reload();
                            }}>Cancel</button>
                            <button className="createButton" onClick={e => { e.preventDefault() }}>Create Room</button>
                        </div>
                    }
                    {
                        (this.state.errorAlert !== '' && this.state.errorAlert !== 'Room Chat Not Found') &&
                        <div>
                            <p className="errorAlert">{this.state.errorAlert}</p>
                            <button className="oneCancelButton" onClick={e => {
                                e.preventDefault()
                                this.props.history.push("/");
                                window.location.reload();
                            }}>Cancel</button>
                        </div>
                    }
                </form>
            </div >
        )
    }
}

import React, { Component } from 'react'
import axios from 'axios'
import utils from './utils'
export class roomChat extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            roomId: '',
            chatLogs: []
        }
    }

    componentDidMount() {
        const username = localStorage.getItem("username")
        const roomId = localStorage.getItem("roomId")

        if (!username && !roomId) {
            this.props.history.push("/join-room");
            window.location.reload();
        }
        this.setState({ username, roomId })

        //** get message */
        const url = utils.BACKEND_URL + `/get-message/${roomId}`
        const getChats = async () => {
            axios.get(url).then(res => {
                this.setState({ chatLogs: res.data.detail })
            })
        }
        getChats();
    }

    render() {
        const chats = this.state.chatLogs.map((chat, index) => {
            return <li className="chat" key={index}>{chat.message}</li>
        })
        return (
            <div>
                <div className="wrapper">
                    <button className="exitButton">Exit</button>
                    <p className="title">{this.state.roomId}</p>
                </div>
                <div className="chatBox">
                    <ul>{chats}</ul>
                </div>
                <form className="chatInput">
                    <input type="text" className="chatInput" placeholder="Send a message..." />
                    <button className="sendChat">↑</button>
                </form>
            </div >
        )
    }
}

export default roomChat